package es.incentro.training.cronjob;

import java.util.List;

import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.springframework.beans.factory.annotation.Autowired;

import es.incentro.training.service.CronJobService;


public class CustomAction extends ActionExecuterAbstractBase {
	
	   private CronJobService cronJobService;


	public static final String NAME = "cron borrar";
	
 public static final String PARAM_ASPECT_NAME = "aspect-name";
 
 /**
 * the node service
 */
 private NodeService nodeService;

 /**
 * Set the node service
 * 
 * @param nodeService
 * the node service
 */
 public void setNodeService(NodeService nodeService) {
 this.nodeService = nodeService;
 }
 
 public void setCronJobService(CronJobService cronJobService) {
	 this.cronJobService = cronJobService;
	 }

 @Override
 protected void executeImpl(Action action, NodeRef actionedUponNodeRef) {
	 cronJobService.DeleteOthers(action, actionedUponNodeRef,nodeService);

 }


 @Override
 protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
  paramList.add(new ParameterDefinitionImpl(PARAM_ASPECT_NAME,
  DataTypeDefinition.QNAME, true,
  getParamDisplayLabel(PARAM_ASPECT_NAME)));

 }

}