package es.incentro.training.rules;



import org.alfresco.api.AlfrescoPublicApi;
import org.alfresco.service.Auditable;
import org.alfresco.service.namespace.NamespacePrefixResolver;

/**
 * Namespace Service.
 * 
 * The Namespace Service provides access to and definition of namespace
 * URIs and Prefixes. 
 *
 */
@AlfrescoPublicApi
public interface namespace0 extends NamespacePrefixResolver
{
	  /** Default Namespace URI */
    static final String ypa_URI = "http://www.mycompany.com/model/content/1.0";
    
    /** Default Namespace Prefix */
    static final String ypa_PREFIX = "ypa";
    
    @Auditable(parameters = {"prefix", "uri"})
    public void registerNamespace(String prefix, String uri);
    
    @Auditable(parameters = {"prefix"})
    public void unregisterNamespace(String prefix);
}