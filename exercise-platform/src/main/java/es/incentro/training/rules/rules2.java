package es.incentro.training.rules;
import java.util.List;

import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.beans.factory.annotation.Autowired;


import es.incentro.training.service.RulesService;

public class rules2 extends ActionExecuterAbstractBase {
   public static final String NAME = "rules2";

private RulesService rulesService;

   public void setRulesService(RulesService rulesService) {
       this.rulesService = rulesService;
   }

   @Override
   protected void executeImpl(Action action, NodeRef node) {
	   rulesService.MoveHtml(action, node);
   }

   @Override
   protected void addParameterDefinitions(List<ParameterDefinition> arg0) {

   }
}

