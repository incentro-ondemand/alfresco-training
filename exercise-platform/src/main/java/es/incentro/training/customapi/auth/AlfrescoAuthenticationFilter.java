package es.incentro.training.customapi.auth;


import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.service.cmr.security.AuthenticationService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotAuthorizedException;

import java.io.IOException;
import java.util.StringTokenizer;

public class AlfrescoAuthenticationFilter implements Filter {

	private FilterConfig filterConfig;

	@Autowired
	private AuthenticationService authenticationService;

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
		// try ticket authentication
		String ticket = servletRequest.getParameter("alf_ticket");
		if (ticket == null || ticket.equals("")) {
			// do basic authentication
			try {
				doBasicAuthentication(servletRequest, servletResponse);
			} catch (NotAuthorizedException e) {
				// authentication failed
				return;
			}
		} else {
			// throws an error if ticket is not valid
			authenticationService.validate(ticket);
		}

		// authentication succeeded
		chain.doFilter(servletRequest, servletResponse);
	}

	private void doBasicAuthentication(ServletRequest servletRequest, ServletResponse servletResponse) throws IOException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		String authHeader = request.getHeader("Authorization");
		if (authHeader == null) {
			setUnauthorizedHeader(response);
		}
		StringTokenizer st = new StringTokenizer(authHeader);
		if (!st.hasMoreTokens()) {
			setUnauthorizedHeader(response);
		}
		String basic = st.nextToken();

		if (!basic.equalsIgnoreCase("Basic")) {
			setUnauthorizedHeader(response);
		}

		String credentials = new String(Base64.decodeBase64(st.nextToken()));
		int p = credentials.indexOf(":");
		if (p == -1) {
			setUnauthorizedHeader(response);
		}
		String userName = credentials.substring(0, p).trim();
		String password = credentials.substring(p + 1).trim();
		
		// validate credentials
		if(userName.equals("ROLE_TICKET")) {
			try {
				//Es la password
				authenticationService.validate(password);
			} catch (AuthenticationException e) {
				setUnauthorizedHeader(response);
			}
		} else {
			try {
				authenticationService.authenticate(userName, password.toCharArray());
			} catch (AuthenticationException e) {
				setUnauthorizedHeader(response);
			}
		}

	}

	private void setUnauthorizedHeader(HttpServletResponse response) throws IOException {
		String realm = "protected";
		response.setHeader("WWW-Authenticate", "Basic realm=\"" + realm + "\"");
		response.sendError(401);
		throw new NotAuthorizedException("Credentials error");
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	@Override
	public void destroy() {
	}
}
