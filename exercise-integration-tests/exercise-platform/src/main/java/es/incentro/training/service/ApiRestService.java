package es.incentro.training.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import javax.transaction.Status;
import javax.transaction.UserTransaction;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import es.incentro.training.customapi.controller.CustomController;
import es.incentro.training.models.Models;

public class ApiRestService {
	
	@Autowired
    private FileFolderService fileFolderService;
	@Autowired
	private SearchService SearchService;
	@Autowired
	private NodeService nodeService;
	@Autowired
	private TransactionService transactionService;
	
	private static final Logger logger = LoggerFactory.getLogger(CustomController.class);
	
	public ResponseEntity<String> actualizar(String name,String newname) {
		logger.debug("changename="+name+" : "+newname);
   	 SearchParameters sp = new SearchParameters();
        sp.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
        sp.setQuery("PATH: 'app:company_home/cm:"+name+"'");
        sp.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        ResultSet rs = SearchService.query(sp);
        if (rs.length()>0) {
       	 NodeRef node = rs.getNodeRefs().get(0);
       	 UserTransaction tx = transactionService.getUserTransaction();
				try {
					tx.begin();
					 FileInfo fileInfo = fileFolderService.rename(node, newname);
					 tx.commit();
					logger.debug("Original:"+name+" nuevo:"+newname);
					return new ResponseEntity<>("Original:"+name+" nuevo:"+newname, HttpStatus.OK);
				} catch (Exception e) {
					logger.debug(e.getMessage());
				}finally {
				try {
					tx.rollback();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.debug(e.getMessage());
				}
				}
        }else {
        logger.debug("La carpeta no existe");
        }
return new ResponseEntity<>("Error!", HttpStatus.OK);
	}
	
	public ResponseEntity<String> borrar(String ref) {
		 
		 UserTransaction tx = transactionService.getUserTransaction();
		 NodeRef node = new NodeRef("workspace://SpacesStore/"+ref);
		 try {
		     tx.begin();
		    nodeService.deleteNode(node);
		    tx.commit();
		    logger.debug("exito");
			return new ResponseEntity<>("Borrado con exito!", HttpStatus.OK);
		 } catch (Throwable e) {
		     try {
		         if (tx.getStatus() == Status.STATUS_ACTIVE) {
		             tx.rollback();
		         }
		     } catch (Exception t) {
		         logger.error("Unable to roll back job.", t);
		     }
		     logger.debug("Error trying to delete node : "+ e.getMessage());    
		 }
		 return new ResponseEntity<>("Error al borrar/No existe", HttpStatus.OK); 
	 }
	
	public ResponseEntity<ArrayList<String>> propiedades(String ref) {
		 NodeRef node = new NodeRef("workspace://SpacesStore/"+ref);
		 ArrayList<String> list = new ArrayList();
		 list.add("Propiedades:");
		 Map<QName, Serializable> siteProperties = nodeService.getProperties(node);
		 list.add("Creador: "+siteProperties.get(ContentModel.PROP_CREATOR));
		 list.add("Creado: "+siteProperties.get(ContentModel.PROP_CREATED));
		 list.add("Nombre: "+siteProperties.get(ContentModel.PROP_NAME));
		 list.add("Tipo: "+siteProperties.get(ContentModel.TYPE_CONTENT));
		 return new ResponseEntity<>(list, HttpStatus.OK);
		 
	 }
	
	public ResponseEntity<String> cambios(String ref,String nombre,String descripcion) {
		 UserTransaction tx = transactionService.getUserTransaction();
		 NodeRef node = new NodeRef("workspace://SpacesStore/"+ref);
		 try {
		     tx.begin();
		     fileFolderService.rename(node, nombre);
		     Map<QName, Serializable> props = nodeService.getProperties(node);
		     props.put(ContentModel.PROP_DESCRIPTION,descripcion);
		     props.put(ContentModel.PROP_TITLE,nombre);
		     nodeService.setProperties(node, props);
		     nodeService.removeAspect(node,Models.LOCATION_CONTENT);
		     tx.commit();
		     return new ResponseEntity<>("Todo correcto", HttpStatus.OK);
		 }catch(Exception e) {
			 try {
				tx.rollback();
			 } catch (Exception q) {
					logger.debug(q.getMessage());
				}
		 }
		 return new ResponseEntity<>("Error!", HttpStatus.OK);
	 }

}
