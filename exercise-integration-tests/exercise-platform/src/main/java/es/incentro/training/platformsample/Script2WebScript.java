/**
 * Copyright (C) 2017 Alfresco Software Limited.
 * <p/>
 * This file is part of the Alfresco SDK project.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package es.incentro.training.platformsample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.MimetypeService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A demonstration Java controller for the Hello World Web Script.
 *
 * @author martin.bergljung@alfresco.com
 * @since 2.1.0
 */
public class Script2WebScript extends DeclarativeWebScript {
    private static Log logger = LogFactory.getLog(Script2WebScript.class);
    
    @Autowired
	   private SearchService SearchService;
	@Autowired
	   private MimetypeService mimetypeService;
	@Autowired
	 private NodeService nodeService;
    
    protected Map<String, Object> executeImpl(
            WebScriptRequest req, Status status, Cache cache) {
        Map<String, Object> model = new HashMap<String, Object>();
        String type = req.getParameter("type");
        logger.debug("Script2 is called!");
        if (type.equalsIgnoreCase("listFilesJob")) {
        	model = listar(type);
        }else if(type.equalsIgnoreCase("deleteJob")){
        	model.put("Archivos", "Archivos de Rejected Documents eliminados");
        }
        return model;
    }
    
    
    
    
 private Map<String, Object> listar(String type) {
	 Map<String, Object> model = new HashMap<String, Object>();
	 String str = "Archivos:\n";
     SearchParameters sp = new SearchParameters();
     sp.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
     sp.setQuery("PATH: 'app:company_home//*'");
     sp.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
     ResultSet rs = SearchService.query(sp);
     if (rs.length()>0) {
    	 List<NodeRef> nodes = rs.getNodeRefs();
    	 for(NodeRef n: nodes) {
    	 final String fileName = (String) nodeService.getProperty(n, ContentModel.PROP_NAME);
		 final String mimetype = mimetypeService.guessMimetype(fileName);
		 		str = str+"File: "+fileName+"\n";
		 	}
    	 }
     model.put("Archivos", str);
     return model;
 }
 
 private void eliminar() {
	 SearchParameters sp = new SearchParameters();
     sp.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
     sp.setQuery("PATH: 'app:company_home/cm:Rejected_x0020_Documents'");
     sp.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
     ResultSet rs = SearchService.query(sp);
     if (rs.length()>0) {
    	 List<NodeRef> n0 = rs.getNodeRefs();
    	 NodeRef nodep = n0.get(0);
    	 
    	 SearchParameters sp2 = new SearchParameters();
         sp2.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
         sp2.setQuery("PATH: 'app:company_home/cm:Rejected_x0020_Documents//*'");
         sp2.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
         ResultSet rs2 = SearchService.query(sp2);
         if (rs2.length()>0) {
        	 List<NodeRef> nodes = rs2.getNodeRefs();
        	 int count = 0;
        	 for(NodeRef n: nodes) {
        		 final String fileName = (String) nodeService.getProperty(n, ContentModel.PROP_NAME);
        		 final String mimetype = mimetypeService.guessMimetype(fileName);
        		 if (!mimetype.equals("text/html") && !mimetype.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
        			 count++;
        			 nodeService.removeChild(nodep, n);
        			 logger.debug("Eliminado: "+fileName);
        		 }
        		 }
        	 logger.debug("Archivos eliminados: "+count);
         }
    	 }
 }
}