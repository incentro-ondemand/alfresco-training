package es.incentro.training.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.UserTransaction;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.MimetypeService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import es.incentro.training.behaviour.PolicyLogger;
import es.incentro.training.models.Models;

public class BehaviourService {
	
	@Autowired
	private SearchService SearchService;
	@Autowired
	private NodeService nodeService;
	@Autowired
	private MimetypeService mimetypeService;
	@Autowired
	private TransactionService transactionService;
	
	private Logger logger = Logger.getLogger(PolicyLogger.class);
	
	public void MoveExcel(ChildAssociationRef arg0) {
		SearchParameters sp = new SearchParameters();
        sp.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
        sp.setQuery("PATH: 'app:company_home/cm:Excel_x0020_files'");
        sp.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        NodeRef node = arg0.getChildRef();
        
        ResultSet rs = SearchService.query(sp);
        final String fileNamex = (String) nodeService.getProperty(arg0.getParentRef(), ContentModel.PROP_NAME);
        if (rs.length()>0 && fileNamex.equals("Rejected Documents")) {
        	NodeRef node2 = rs.getNodeRefs().get(0);
        	final String fileName = (String) nodeService.getProperty(node, ContentModel.PROP_NAME);
     	   final String mimetype = mimetypeService.guessMimetype(fileName);
     	  if(mimetype.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
        	nodeService.moveNode(node,node2,null,null);
        	UserTransaction tx = transactionService.getUserTransaction();
        	try {
        		tx.begin();
        	Map<QName, Serializable> props = new HashMap<QName, Serializable>(2);
            props.put(Models.PLOCATION_CONTENT,  node.toString());
        	  nodeService.addAspect(node,Models.LOCATION_CONTENT, props);
              nodeService.moveNode(node,node2,null,null);
              logger.debug("Node1store="+node.getStoreRef().toString());
              if (logger.isDebugEnabled()) {
      			logger.debug("Node Moved");
      		}
              tx.commit();
        	}catch(Exception e) {
        		try {
					tx.rollback();
				} catch (Exception q) {
					// TODO Auto-generated catch block
					logger.debug(q.getMessage());
				}
        	}
     	  }
        
		
        }
	}

}
