package es.incentro.training.customapi.controller;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.forms.FormException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.AuthenticationUtil.RunAsWork;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.InvalidNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AuthorityService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import es.incentro.training.models.Models;
import es.incentro.training.service.ApiRestService;
import es.incentro.training.service.CronJobService;

@Controller
@RequestMapping("/custom")
public class CustomController {
	
	@Autowired
	   private ApiRestService apiRestService;
	
	
	private static final Logger logger = LoggerFactory.getLogger(CustomController.class);
	
	 @RequestMapping(value = "/test/{name}", method = RequestMethod.GET)
	 @ResponseBody
	    public ResponseEntity<String> getValueList(@PathVariable("name") String name) {
	    	logger.debug("GG2="	+name);
	    	return new ResponseEntity<>("Recibido: "+name, HttpStatus.OK);

	    }
	 
	 @PutMapping(value = "/changename/{name}/{newname}")
	 @ResponseBody
	 public ResponseEntity<String> actualizar(@PathVariable("name") String name,@PathVariable("newname") String newname) {
		 return apiRestService.actualizar(name, newname);
	    	
}
	 @DeleteMapping(value = "/borrar/{ref}")
	 @ResponseBody
	 public ResponseEntity<String> borrar(@PathVariable("ref") String ref) {
	 return apiRestService.borrar(ref);
	 }
	 
	 @RequestMapping(value = "/propiedades/{ref}", method = RequestMethod.GET)
	 @ResponseBody
	    public ResponseEntity<ArrayList<String>> propiedades(@PathVariable("ref") String ref) {
		return this.apiRestService.propiedades(ref);
	 }
	 
	 @RequestMapping(value = "/cambios/{ref}/{nombre}/{descripcion}", method = RequestMethod.POST)
	 @ResponseBody
	 public ResponseEntity<String> cambios(@PathVariable("ref") String ref,@PathVariable("nombre") String nombre,@PathVariable("descripcion") String descripcion) {
		 return this.apiRestService.cambios(ref, nombre, descripcion);
	 }
	 
}
